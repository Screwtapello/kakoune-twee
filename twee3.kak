hook global BufCreate .+\.tw(ee)? %{
    set-option buffer filetype twee3
}

hook global WinSetOption filetype=twee3 %{
    require-module twee3

    add-highlighter window/twee3 ref twee3
    hook -once -always window WinSetOption filetype=.* %{
        remove-highlighter window/twee3
    }

    try %{
        # Look for the StoryData JSON blob and select it
        execute-keys '%s^::StoryData<ret>m'
        # What format is this story in?
        try %{
            execute-keys s"format":\s"Harlowe"<ret>
            # Yep, it's Harlowe!
            add-highlighter window/twee3harlowe ref twee3harlowe
            hook -once -always window WinSetOption filetype=.* %{
                remove-highlighter window/twee3harlowe
            }
        }
    }
}

provide-module twee3 %&
    add-highlighter shared/twee3 group
    add-highlighter shared/twee3/ regex '^(::)([^[\n{]*)(\[[^\n{]*)?(\{[^\n]*)?' \
        1:keyword 2:header 3:meta 4:attribute

    add-highlighter shared/twee3harlowe regions

    # Links between passages
    add-highlighter shared/twee3harlowe/link region \[\[ \]\] group
    add-highlighter shared/twee3harlowe/link/ fill link
    add-highlighter shared/twee3harlowe/link/ regex <-|-> 0:operator
    add-highlighter shared/twee3harlowe/link/ regex \[\[|\]\] 0:keyword

    # Macros
    add-highlighter shared/twee3harlowe/macro region \
        -recurse \( \
        \([A-Za-z-]+: \
        \) \
        regions
    add-highlighter shared/twee3harlowe/macro/string1 region '"' '"' fill string
    add-highlighter shared/twee3harlowe/macro/string2 region "'" "'" fill string
    add-highlighter shared/twee3harlowe/macro/expression default-region group
    add-highlighter shared/twee3harlowe/macro/expression/ fill operator
    add-highlighter shared/twee3harlowe/macro/expression/ regex \(([A-Za-z-]+) 1:keyword
    add-highlighter shared/twee3harlowe/macro/expression/ regex \b(to|into|is|not|where|via|making)\b 1:keyword
    add-highlighter shared/twee3harlowe/macro/expression/ regex (\$|_)[A-Za-z][A-Za-z0-9]* 0:variable
    add-highlighter shared/twee3harlowe/macro/expression/ regex (\+|-)?[0-9](\.[0-9]+)? 0:value
    add-highlighter shared/twee3harlowe/macro/expression/ regex true|false 0:value

    # All regular text formatting
    add-highlighter shared/twee3harlowe/text default-region group

    # Line continuations
    add-highlighter shared/twee3harlowe/text/ regex \\$ 0:WrapMarker

    # In-line text formatting
    add-highlighter shared/twee3harlowe/text/ regex "//[^/\n]+?//" 0:italic
    add-highlighter shared/twee3harlowe/text/ regex "''[^'\n]+?''" 0:bold
    #add-highlighter shared/twee3harlowe/text/ regex "~~[^~\n]+?~~" 0:strikethrough
    add-highlighter shared/twee3harlowe/text/ regex "(?<!\*)\*[^*\n]+?\*(?!\*)" 0:italic
    add-highlighter shared/twee3harlowe/text/ regex "\*\*[^*\n]+?\*\*" 0:bold
    add-highlighter shared/twee3harlowe/text/ regex "`[^`\n]*?`" 0:mono

    # Hooks
    add-highlighter shared/twee3harlowe/text/ regex ((?<!\[)|\|[A-Za-z][A-Za-z0-9]*(\)|>))\[(?!\[) 0:meta
    add-highlighter shared/twee3harlowe/text/ regex (?<!\])\](?!\])((\(|<)[A-Za-z][A-Za-z0-9]*\|)? 0:meta
&
