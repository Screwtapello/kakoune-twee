Twee syntax highlighting for Kakoune
====================================

[Twine] is a popular tool for writing interactive fiction
that can be published as an ordinary webpage
and played in a web browser.
However,
while the standard Twine editor is easy to get started with,
some of us are used to more primitive and powerful editing tools.

As an alternative to the standard tool,
[Tweego] is a command-line compiler
that reads a story in the [Twee v3] format
and combines it with a standard Twine story format engine
to create the same HTML output
that the regular Twine tool would generate.

[Twine]: https://twinery.org/
[Tweego]: https://www.motoslave.net/tweego/
[Twee v3]: https://github.com/iftechfoundation/twine-specs/blob/master/twee-3-specification.md

Once you've got Tweego installed,
you can write Twee files in Kakoune like any other plain-text format.
But like any other plain-text format,
it's a bit more pleasant with some syntax-highlighting.

Here's a screenshot of [the included demo](demo.twee)
with syntax highlighting:

![A screenshot with syntax highlighting](demo.png)

Features
========

  - Highlights Twee passage headers, metadata and attributes
    for all Twine story formats
  - For the Harlowe story format (Twine's default),
    also highlights text formatting, links, macros and hooks


Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `twee3.kak` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-twee.git

Configuration
=============

This plugin provides no configuration options.

Commands
========

This plugin provides no custom commands.

Usage
=====

After the plugin is installed,
editing a `*.tw` or `*.twee` file should automatically enable highlighting.

TODO
====

  - Add highlighting for the more esoteric corners of the Harlowe syntax
      - Complete existing passage names inside links?
  - Support story formats other than Harlowe
